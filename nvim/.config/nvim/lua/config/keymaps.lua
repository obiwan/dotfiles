-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local keymap = vim.keymap
local opts = { noremap = true, silent = true }

-- Delete a word backwards
keymap.set("n", "dw", 'vb"_d', opts)
-- Create newline below and leave insert mode
keymap.set("n", "<leader>j", "o<esc>", opts)
-- Move to the beginning of the line - Works nice but conflicts with keymap to switch windows
-- keymap.set("n", "H", "^", opts)
-- Move to the end of the line - Works nice but conflicts with keymap to switch windows
-- keymap.set("n", "L", "$", opts)
-- Duplicate a line and comment out the first line
keymap.set("n", "yc", "yygccp")

-- Incremental search bind
keymap.set("n", "<leader>rn", ":IncRename ")

keymap.set("n", "<leader>cx", vim.diagnostic.reset, opts)
-- keymap.set("n", "<leader>rn", function()
--   return ":IncRename " .. vim.fn.expand("<cword>")
-- end, { expr = true })

-- clean ^Ms (windows newlines)
keymap.set("n", "<leader>m", function()
  vim.cmd([[silent! %s/\r//g]])
  vim.cmd([[w]])
  vim.notify("Cleaned all newline characters!", vim.log.levels.INFO, { title = "File Saved" })
end, { remap = false, desc = "Clean ^M", silent = true })
