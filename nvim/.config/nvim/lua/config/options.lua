-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt

opt.wrap = true
opt.expandtab = true

-- vim.g.snacks_animate = false -- Disable cursor animations
-- vim.opt.textwidth = 80 -- Set textwidth to 80 - will cause linebreaks at 80 characters
