return {
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "tokyonight",
      -- version = "v14.6.0",
    },
  },
  -- Configure LazyVim to load specific colorscheme
  {
    "folke/tokyonight.nvim",
    lazy = true,
    opts = {
      style = "storm",
    },
  },
}
