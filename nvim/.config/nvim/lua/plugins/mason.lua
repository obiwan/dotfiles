return {
  -- add any tools you want to have installed below
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "prettier",
        "stylua",
        "shellcheck",
        "shfmt",
        "flake8",
        "markdown-toc",
        "markdownlint-cli2",
      },
    },
  },
}
