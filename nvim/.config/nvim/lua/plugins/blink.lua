return {
  -- completion plugin with support fort LSPs and external sources that updates
  -- on every keystroke with minimal overhead
  --
  -- https://www.lazyvim.org/extras/coding/blink
  -- Documentation site: https://cmp.saghen.dev/
  -- {
  --   "giuxtaposition/blink-cmp-copilot",
  -- },
  --
  {
    "saghen/blink.cmp",
    enabled = true,
    ---@module 'blink.cmp'
    ---@type blink.cmp.Config
    opts = {
      -- This is where the change happens:
      -- 1) Add `preset = "luasnip"` so blink.cmp knows you're using Luasnip
      -- 2) Remove the explicit `luasnip` source from sources.providers
      snippets = {
        preset = "luasnip",
        expand = function(snippet, _)
          return LazyVim.cmp.expand(snippet)
        end,
      },

      appearance = {
        use_nvim_cmp_as_default = false,
        nerd_font_variant = "mono",
      },
      completion = {
        accept = {
          auto_brackets = { enabled = true },
        },
        menu = {
          draw = { treesitter = { "lsp" } },
        },
        documentation = {
          auto_show = true,
          auto_show_delay_ms = 200,
        },
        ghost_text = {
          enabled = vim.g.ai_cmp,
        },
      },

      sources = {
        compat = {},
        default = { "lsp", "path", "snippets", "buffer" },
        providers = {
          lazydev = {
            name = "LazyDev",
            module = "lazydev.integrations.blink",
            score_offset = 1000,
          },
          lsp = {
            name = "lsp",
            enabled = true,
            module = "blink.cmp.sources.lsp",
            kind = "LSP",
            score_offset = 999,
          },
          snippets = {
            name = "snippets",
            enabled = true,
            module = "blink.cmp.sources.snippets",
            score_offset = 900,
          },
          copilot = {
            name = "copilot",
            enabled = vim.g.ai_cmp,
            module = "blink-cmp-copilot",
            kind = "Copilot",
            score_offset = -100,
            async = true,
          },
        },
        cmdline = {},
      },
      keymap = {
        preset = "super-tab",
        ["<C-z>"] = { "hide", "fallback" },
      },
    },
  },
}
