# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  z
  terraform
  # zsh-vi-mode
  zsh-fzf-history-search
  zsh-autosuggestions
  fast-syntax-highlighting
  zsh-completions
  # zsh-autocomplete # If zsh autocomplete is loaded before syntax-highlighting, some warnings occur on loading
	git
  docker
  docker-compose
  # kubectl
  # python
  # azure
	safe-paste
)

# Some modification on fzf reverse history search
ZSH_FZF_HISTORY_SEARCH_EVENT_NUMBERS=0
ZSH_FZF_HISTORY_SEARCH_DATES_IN_SEARCH=0
ZSH_FZF_HISTORY_SEARCH_REMOVE_DUPLICATES=true

HISTSIZE=10000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

source $HOME/.oh-my-zsh/oh-my-zsh.sh

# User configuration
prompt_context(){
  prompt_segment black default
}

# Go path variables
export GOROOT="/usr/local/go"
export GOPATH="$HOME/go"

# Update PATH to include GOPATH and GOROOT binaries
export PATH=$GOPATH/bin:$GOROOT/bin:$HOME/.local/bin:$PATH

export PATH="$PATH:/usr/local/texlive/2021/bin/x86_64-linux"
export PATH="$PATH:$HOME/.npm-global/bin"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# WSL2 Fedora ansible ca bundle path
export REQUESTS_CA_BUNDLE=/etc/pki/tls/certs/ca-bundle.crt

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='mvim'
fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

# Disable zsh nomatch option
setopt +o nomatch

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias v='nvim'
alias vim='nvim'
alias g='git'
alias lh="ls -lah"
alias tping='ping -c 3 8.8.8.8'
alias vimconfig="nvim $HOME/.config/nvim/init.vim"
alias zshconfig="nvim $HOME/.zshrc"
alias ohmyzsh="nvim $HOME/.oh-my-zsh"
alias reload="source $HOME/.zshrc"
alias yt-audio-only="yt-dlp --extract-audio --audio-format mp3"
alias nc='ncat'
# Dotfiles git repo alias
# alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME' (replaced by stow)
alias d='docker'
alias tf='terraform'
alias tfp='terraform plan'
alias tfa='terraform apply'
alias tfi='terraform init'

# Bat alias for cat replacement
alias cat='bat'

# Nautilus shortcut
alias nt='nautilus'
alias open='xdg-open'

# alias awsauth='_authaws(){bash ~/.config/nixpkgs/scripts/aws-set-auth.sh "$1" && . ./tmp.env && rm ./tmp.env;}; _authaws'

# Bind autosuggest-accpet to ctrl+space
bindkey '^ ' autosuggest-accept

# Streamlink shorts
twitchStream(){ streamlink https://www.twitch.tv/$1 best --player /usr/bin/vlc --twitch-low-latency --twitch-disable-ads --twitch-disable-hosting }
twitchVod(){ streamlink https://www.twitch.tv/videos/$1 best --player /usr/bin/vlc --player-passthrough hls }
watchStream(){ streamlink $1 best --player /usr/bin/vlc --player-passthrough hls }

# Create a cache folder if it isn't exists
if [ ! -d "$HOME/.cache/zsh" ]; then
    mkdir -p $HOME/.cache/zsh
fi

# Define a custom file for compdump
export ZSH_COMPDUMP="$HOME/.cache/zsh/zcompdump-$HOST-$ZSH_VERSION"


# Set up fzf key bindings and fuzzy completion
source <(fzf --zsh)
# Zoxide shell integration and cd replacement
eval "$(zoxide init --cmd cd zsh)"

# Setting fd as the default source for fzf
export FZF_DEFAULT_COMMAND='fd --type f --strip-cwd-prefix'
# fzf boarder config
export FZF_DEFAULT_OPTS="-m --height 70% --border"

alias inv='nvim $(fzf --preview "bat --color=always --style=header,grid --line-range :500 {}")'

# Completion styling
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
# zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# zstyle ':completion:*' menu no
# zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
# zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'
# Disables special dirs like . and ..
zstyle ':completion:*' special-dirs false


# Fix zsh-autosuggestions accept suggestion with ctrl+space
# function zvm_after_init() {
#   zvm_bindkey viins '^@' autosuggest-accept
# }

# AWS
complete -C '/usr/local/bin/aws_completer' aws
complete -o nospace -C /usr/bin/terraform terraform

# Allow hidden folders to be in completions
setopt globdots
# Load completions
autoload -Uz compinit && compinit

# Rebind clear screen to ctrl+o since tmux-vim plugin uses ctrl-l for navigation
bindkey '^O' clear-screen

# NVIM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# spicetify
export PATH=$PATH:/home/obiwan/.spicetify
