# Dotfiles for my personal setup managed with GNU Stow

## Installation on Fedora

```bash
sudo dnf install stow
```

## Usage

After cloning the repository, `cd` into the repository and run the following command to install the dotfiles. This will create symlinks in your home directory to the files in this repository.

```bash
stow -vt ~ zsh git nvim tmux openssh alacritty rsync ublk
```

To install only the dotfiles for a specific program, run the following command:

```bash
stow -vt ~ <program>
```

There might be an issue with generated files within your config directory which will polute your git repo. To fix this, run the following command when installing the dotfiles:

```bash
stow --no-folding -vt ~ <program>
```

Adapting the configs on a system where the files already exists will run into an error, a neat trick can be applied by using the dangerous `--adapt` parameter, which will override the files from the repo and create the desired symlinks. If we follow this with a git restore on the desired stow packages, we should be fine.

```bash
z dotfiles
stow --adopt -vt ~ hypr
git restore .
```

This will create the symlinks without folding the directories (making the directories symlinks themselves).

## List of programs

- zsh
- git
- nvim
- tmux
- openssh
- alacritty
- rsync
- ublk

```
